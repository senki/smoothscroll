# SmoothScroll #

Performs a smooth page scroll to an anchor on the same page.  
A JQuery plugin.

This code is borrowed from [css-trick.com](http://css-tricks.com/snippets/jquery/smooth-scrolling/).  
Originally developed by [Karl Swedberg](http://www.learningjquery.com/2007/10/improved-animated-scrolling-script-for-same-page-links).  
Minified with [YUI Compressor](http://www.refresh-sf.com/yui/)  
Taken on 2013.07.17.
